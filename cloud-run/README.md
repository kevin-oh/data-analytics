# Factory Simulator 

This nodejs app is the simulator of sensors at factory. 23 data point in this application (defined in simulator-config.json).

![Factory demo scenario](../images/AgTWdTP8npNTz5z.png)

## npm install 

Install npm packages
```bash
❯ npm install 
```

## Create pub/sub topic
```bash
❯ gcloud auth application-default login
❯ gcloud pubsub topics create factory-topic-011
❯ gcloud pubsub subscriptions create factory-sub --topic factory-topic-011
```

## Config ADC(Application Default Credentials)

For local dev environment: 
```bash
❯ gcloud auth application-default login
❯ cat ~/.config/gcloud/application_default_credentials.json
{
  "client_id": "<client id>",
  "client_secret": "<client secret>",
  "quota_project_id": "<project id>",
  "refresh_token": "<token>",
  "type": "authorized_user"
}
```
It makes application_default_credentials.json file. 

## local test 
```bash
❯ npm start
```

## Build docker image and run/test it local environment with docker 
```bash
❯ docker build . -t <docker hub id>/factory-simulator-app:1.0.0
❯ docker images
❯ docker run -d -e CLOUD_RUN_TASK_INDEX=0  <docker hub id>/factory-simulator-app:1.0.0 simulator-config.json  
```


## Build image and push to docker hub

For the Mac M1 device, we need ***--platform*** options to run it on Cloud Run. Docker images has it's own architecture. 
```bash 
❯ docker build . --platform=linux/amd64 -t <docker hub id>/factory-simulator-app:1.0.0
❯ docker image inspect <docker hub id>/factory-simulator-app:1.0.0 | grep Architecture
```

need to login with your docker hub account, or you can use another container registry. 
```
❯ docker login 
```

### The completed image is pushed in docker hub 

```bash 
❯ docker pull kevinoh/factory-simulator-app:1.1.2
```


## Deploy to Cloud Run by CLI 

### Create a service account 

```bash
 ❯ gcloud iam service-accounts create factory-simulator-sa --display-name="Factory simulator service account"
```

### Grant "Pub/Sub Publisher" role to the service account

It can be used to send a message to pub/sub. 

```bash
❯ gcloud project add-iam-policy-binding <Project ID> --role roles/pubsub.publisher --member serviceAccount:factory-simulator-sa@<Project ID>.iam.gserviceaccount.com
```

### Depoy a Cloud Run Job 

This command is image base deployment with 23 tasks. Each task'll run only 10 minutes long. Without ***--task-timeout*** 1hr is maxinum at this time, Apr 2023. Don't forget service account. The command for excute a simulator is defined in Dockerfile, but ***args*** is needed in the code. 

```bash
❯ gcloud beta run jobs deploy factory-simulator \ 
   --image <docker hub id>/factory-simulator-app:1.0.0 \
   --args="simulator-config.json" \
   --tasks=23 \
   --task-timeout=10m \
   --region=us-central1 \
   --service-account=factory-simulator-sa@<Project ID>.iam.gserviceaccount.com
```

### Run job

```bash
❯ gcloud beta run jobs list
❯ gcloud beta run jobs execute factory-simulator --region=<region>
✓ Creating execution...Done            
✓ Provisioning resources...Done.                                     
Execution [factory-simulator-wx27k] has successfully started running.
```

![Run at Cloud Run](../images/6YkkjsYYZxHZCnx.png)

## Trouble Shoot 

### Check your image architecture 

Cloud Run has error when I build my image on the MAC M1 (ARM architecture)

![Error: image architecture](../images/4wYb4dGX9YMWsHr.png)



