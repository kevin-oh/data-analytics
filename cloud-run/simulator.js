import { PubSub } from '@google-cloud/pubsub'
import * as fs from 'fs'
import { v4 as uuidv4 } from 'uuid'

const pubSubClient = new PubSub()
const publishMessage = async (topicNameOrId, msg) => {
    const databuffer = Buffer.from(msg)

    try {
        const messageId = await pubSubClient.topic(topicNameOrId).publishMessage({ data: databuffer })
        console.log(`Sending message (${messageId}): ${msg}`);
    } catch (error) {
        console.error(`Received error while publishing: ${error.message}`)
        process.exitCode = 1;
    }
}

// generate rand value
const getRandom = (min, max, decimals) => ((Math.random() * (max - min + 1)) + min).toFixed(decimals)

const main = async (argv) => {
    // file not found
    const filename = argv[0]
    if (!filename) throw new Error("Simulator config file is not found.")

    // Get configuration from file 
    const jsonFile = fs.readFileSync(argv[0], 'utf8')
    const configData = JSON.parse(jsonFile)

    const simulatorName = configData.name
    const topicName = configData.topicName
    const simulators = configData.simulators

    console.log("# of simulator: ", simulators.length)
    console.log("CLOUD_RUN_TASK_INDEX: ", process.env.CLOUD_RUN_TASK_INDEX)

    const taskIndex = process.env.CLOUD_RUN_TASK_INDEX

    // task index check
    if (taskIndex === undefined || taskIndex < 0 || taskIndex >= simulators.length) 
        throw new Error("The # of tasks must be matched with # of simulators in config file")

    // simulator config
    const simulator = simulators[taskIndex]

    console.log(`Start simulator ${simulatorName} ${simulator.name}: generate value between ${simulator.min} and ${simulator.max} every ${simulator.interval} second`)

    const sendInterval = setInterval(function () {
        // generate rand number between min and max
        var value = getRandom(simulator.min, simulator.max, 2)
    
        var data = JSON.stringify({
            tagName: simulator.name,
            eventTimestamp: new Date(),
            value: value,
            id: uuidv4()
        })
    
        publishMessage(topicName, data).catch(err => {
            console.error(err.message)
            throw new Error(`pub/sub error: ${err.message}`)
        });
    // interval in sec
    }, simulator.interval * 1000);
}

// Start here
main(process.argv.slice(2)).catch((err) => {
    console.error(JSON.stringify({ severity: "ERROR", message: err.message }));
    process.exit(1);
})